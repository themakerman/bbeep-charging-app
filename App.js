import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import BbeepMapView from './src/containers/BbeepMapView';


export default class App extends React.Component {

  componentWillMount() {
    const config = {
      apiKey: "AIzaSyDqU7fm8S7pRTgQQLygzg2zitvF-ozvqTY",
      authDomain: "bbeepscooter.firebaseapp.com",
      databaseURL: "https://bbeepscooter.firebaseio.com",
      projectId: "bbeepscooter",
      storageBucket: "bbeepscooter.appspot.com",
      messagingSenderId: "1014235366917"
    };
    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <BbeepMapView />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
