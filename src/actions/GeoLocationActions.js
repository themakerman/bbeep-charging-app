import _ from 'lodash';
import {
  FETCH_SCOOTERS_SUCCESS,
  MARKER_PIN_FETCH_SUCCESS,
} from './types';
import firebase from 'firebase';
import { Constants, Location, Permissions } from 'expo';

//Constants
const TRACK_ONCE_GEOLOCATION_OPTIONS = { enableHighAccuracy: true, maximumAge: 10000 };

//Get user intial position for setting markerPin to user position
export const getPinLocation = () => {
  return async (dispatch) => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    const location = await Location.getCurrentPositionAsync(TRACK_ONCE_GEOLOCATION_OPTIONS);
    const { accuracy, altitude, latitude, longitude, speed, heading  } = location.coords;
    dispatch({ type: MARKER_PIN_FETCH_SUCCESS, payload: location.coords });
  }
}

//Get all live scooters on the field
export const fetchLiveScootersOnField = () => {
  return (dispatch) => {
    firebase.database().ref('/scooters/')
    .on('value', (snapshot) => {
      dispatch({ type: FETCH_SCOOTERS_SUCCESS, payload: snapshot.val()})
    })
  }
}
