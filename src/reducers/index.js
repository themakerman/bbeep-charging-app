import { combineReducers } from 'redux';
import GeoLocationReducer from './GeoLocationReducer';


export default combineReducers({
  geoLocation: GeoLocationReducer
})
