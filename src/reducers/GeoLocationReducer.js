import {
  FETCH_SCOOTERS_SUCCESS,
  MARKER_PIN_FETCH_SUCCESS
} from '../actions/types';

const INITIAL_STATE = { markerPin: null, liveScooters: null };

const geoLocationReducer = (state=INITIAL_STATE, actions) => {
  switch(actions.type) {
    case MARKER_PIN_FETCH_SUCCESS:
      return { ...state, markerPin: actions.payload }
    case FETCH_SCOOTERS_SUCCESS:
      return { ...state, liveScooters: actions.payload }
    default:
      return { ...state };
  }
}

export default geoLocationReducer;
