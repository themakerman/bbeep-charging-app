import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  Dimensions,
  Animated
 } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
import { connect } from 'react-redux';
import { MapView } from 'expo';
import { getPinLocation, fetchLiveScootersOnField } from '../../src/actions/';
import _ from 'lodash';
import { Ionicons } from '@expo/vector-icons';
import MarkerPin from '../../src/components/MarkerPin';


//WINDOW_MEASUREMENTS
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

//INITIAL COUNTRY - SINGAPORE COORDINATES
const LATITUDE = 1.351616;
const LONGITUDE = 103.808053;


class BbeepMapView extends React.Component {

  state = { animateToUserLocation: true }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getPinLocation();
    this.props.fetchLiveScootersOnField();
  }

  componentWillReceiveProps(nextProps) {
    if(!nextProps.markerPin) {
      console.log('Marker still not received');
    }
    else {
      console.log('Marker received');
      if(this.state.animateToUserLocation) {
        this.mapView.animateToCoordinate({'latitude': nextProps.markerPin.latitude,'longitude': nextProps.markerPin.longitude}, 1000);
        this.setState({ animateToUserLocation: false });
      }
    }
  }

  getMapRegion() {
    return (
      region = {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: 0.1,
        longitudeDelta: 0.05,
      }
    )
  }


  mapScootersOnField() {
    if(!this.props.scootersOnField) {
      return
    } else {
      return this.props.scootersOnField.map((scooters) => {
        const coord = {'latitude': scooters.lat, 'longitude': scooters.lng };
        return (
          <MapView.Marker
            key={scooters.scooterId}
            coordinate={coord}
          >
            <View style={{ width: 20, height: 20, borderWidth: 3, borderColor: scooters.batteryPercentage<=15? 'indigo': 'orange', borderRadius: 20, backgroundColor: 'white' }} />
            <MapView.Callout style={{ width: WINDOW_WIDTH*0.45, height: WINDOW_HEIGHT*0.2 }}>
              <Text>Lat: {scooters.lat}</Text>
              <Text>Lng: {scooters.lng}</Text>
              <Text>Battery: {scooters.batteryPercentage} %</Text>
            </MapView.Callout>
          </MapView.Marker>
        );
      })
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MapView
          style={{ flex: 1, width: WINDOW_WIDTH, height: WINDOW_HEIGHT }}
          ref={(mapView) => { this.mapView = mapView }}
          initialRegion={this.getMapRegion()}
          followsUserLocation={this.state.animateToUserLocation}
          showsUserLocation={true}
          showsMyLocationButton={true}
          minZoomLevel={5}
          maxZoomLevel={20}
          showsCompass={true}
          loadingEnabled={true}
        >
          {this.mapScootersOnField()}
        </MapView>
        <MarkerPin />
      </View>
    );
  }
}

const mapStateToProps = ({ geoLocation }) => {
  const { markerPin, liveScooters } = geoLocation;
  const scootersOnField = _.map(liveScooters, (val) => {
    return { ...val };
  })
  return { scootersOnField, markerPin };
};

export default connect(mapStateToProps, { getPinLocation, fetchLiveScootersOnField })(BbeepMapView);
