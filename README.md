# Bbeep Charging App

Bbeep Charging App for demonstrating charging map for escooter sharing businesses

# Task

This is coding assignement for the escooter ride hailing company - https://www.ridebeam.com/

# Problem Statement
**Part 1:** (coding) Fetching X Scooters within a radius (Y).

Your goal is to code a system that can fetch the closest x scooters for a given lat,lon within y meters and plot it on a map. This exercise will include front-end, backend and data storage, and you can choose any technology you’d like for each.

Steps:

1. Create the data storage for the scooters and populate data for scooters placed randomly throughout Singapore.


2. Write a backend service that can provide data to the front end for plotting the closest x scooters within y meters of a given lat,lon


3. Write front end that allows you to set: x scooters, y search radius, lat, and long


It should fetch the data based on these params from the backend service, and it should plot on a map.
Documentation: Be sure to include sample values for x, y, lat, long for testing and any notes needed to test your exercise.
Submission: You can submit to a private repo in Bitbucket or send us a zip file. Please include any instructions needed to quickly test.
Tip: We care a lot about code quality and understandability.

**Part 2:** (technical design) Let’s say that we have plot all the scooters in Singapore on a map (hundreds of thousands) for our operations team to understand where all the scooters currently are. Please describe how you would do that, focusing on usability, performance and scalability. You can focus more on the front end, the backend or discuss both equally in your answer. (~1 page should be fine, but feel free to use more space if needed.)


**Part 3:** ( optional) Let us know in a cover letter what excites you right now about engineering or working on the Beam engineering team.
